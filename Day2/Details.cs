﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    struct product
    {
        public int Id;
        public string productName;
        public string productCategory;

        public product(int Id , string productName, string productCategory)
        {
                this.Id = Id;
                this.productName = productName;
                this.productCategory = productCategory;
        }
        
        
    }
}
